SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[puzic] (
		[col1]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[col2]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[puzic] SET (LOCK_ESCALATION = TABLE)
GO
